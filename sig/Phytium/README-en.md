## Phytium SIG

Phytium SIG provides continuous kernel adaptation support and performance optimization for platforms equipped with Phytium high-performance processors.

## Goal-setting

1. Maintain the Phytium kernel patch code that has been merged into the branch, improve the support of the Phytium product line, and submit kernel driver related patches.
2. Develop, optimize, and adapt Phytium self-developed open source software.
3. Establish the Phytium-OpenKylin mutual certification joint laboratory, and synchronize the mutual certification results of the joint laboratory in the SIG.
4. Integrate Phytium's industrial partners to realize the three-dimensional structure of members in the group and the diversification of resources and solutions.

## SIG Member

## Owner
Hongbo Mao (`maohongbo@phytium.com.cn`)

## Maintainers
Jiakun Shuai (`shuaijiakun1288@phytium.com.cn`)

## SIG Maintenance Packages List

## Mailing List
phytium@lists.openkylin.top
