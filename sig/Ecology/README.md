 **Ecology SIG** 

openKylin社区生态委员会相关工作依托Ecology SIG开展。


 **工作目标**

Ecology SIG作为openKylin社区生态委员会开展工作的载体，生态委员会的职责是构建广泛生态，提升社区品牌影响力，本SIG的工作目标为：

生态共建：拓展生态，共同孵化及打造创新解决方案；

品牌推广：开源文化布道、社区品牌运营、会员宣传矩阵打造；


 **SIG成员** 

SIG Owner： 
 
  James_2016 


SIG Maintainer：

James_2016

Judyzhu741011


 **邮件列表** 

ecology@lists.openkylin.top

 **SIG组例会** 

两月/次例会；