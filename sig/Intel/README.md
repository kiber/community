# Intel Special Interest Group (SIG)

Intel-SIG: Intel-SIG will focus on enabling Intel platform in openKylin OS.

# objectectives:
- Enabling Intel platforms in openKylin OS.
Intel will continually release new platform and keep sync with upstream and openKylin OS based on customer request.
Also Intel will bring more features into openKylins OS such as openVino, oneAPI, Intel GPUs if possible.
Intel like cooperate with openKylin community to support OEMs including Dell, HP, Lenovo and more to support Intel platforms.

## SIG  members

### Owner
- quanxianwang (quanxian.wang@intel.com)

### Maintainers
- quanxianwang (quanxian.wang@intel.com)

### Intel SIG packages
- linux
When enabling Intel new platform in current kernel, maybe we need a bunch of patches. We need branch permission to collect all patches. Before merging into mainline, Intel will review and testing all patches needed and confirm no side effect for mainline. For small patches set, we will directly submit into mainline for review.
- linux-firmware
Same as above. To enable Intel platform, firmwire updates will includes graphics, audio, wifi and others.

### Intel SIG mail list
- [intel@lists.openkylin.top](mailto:intel@lists.openkylin.top)

### Intel SIG regular meeting
- monthly meeting
