# RustKernel SIG

openKylin RustKernel SIG致力于探索Rust语言特性对操作系统内核设计的支持，基于Rust语言实现新型内核及内核特性，并面向新型Rust内核构建软硬件生态。

## 工作目标

- 探索Rust语言特性对操作系统内核设计的支持

- 基于Rust语言实现新型内核及内核特性

- 面向新型Rust内核构建软硬件生态

## SIG成员

 ### Owner
- [maclin](https://gitee.com/kdlinux)
- [Jay](https://gitee.com/zhouyangjia)
- [XZ](https://gitee.com/cynz)

### Maintainers
- [yingqin](https://gitee.com/yingqin2001)
- [lixiaoling1234](https://gitee.com/lixiaoling1234)
- [ddjoyce](https://gitee.com/ddjoyce115)
- [圣西罗的圣卡卡](https://gitee.com/san-kaka-of-san-siro)
- [yiyueming](https://gitee.com/tiger_paper)
- [zhouzhou](https://gitee.com/zhouByFrank)
- [汤佳伟](https://gitee.com/tang-jiaweiiiii)
- [hzyang](https://gitee.com/hzyang0)

## SIG邮件列表
rustkernel@lists.openkylin.top
