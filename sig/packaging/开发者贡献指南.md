# 开发者贡献指南

本文档通过一个简单的实例来演示普通开发者如何为openkylin社区软件包做开发贡献。

文档基于[debhello](https://gitee.com/openkylin/debhello)软件包作为演示。

1. fork源代码到个人用户空间中

浏览器访问[debhello](https://gitee.com/openkylin/debhello)软件包仓库，fork源代码到个人用户空间中

2. 克隆个人用户仓库源码到本地

```sh
git clone git@gitee.com:lllcky/debhello.git
```

3. 修改源码

在这里，开发者可以进行源码的修改。

例如，这里为`debhello`新增个功能，能够根据系统的语言环境打印对应语言的文本。

`src/hello.c`修改如下:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
    const char* lang = getenv("LANG");
    if (strcmp(lang, "zh_CN.UTF-8") == 0) {
        printf("你好，开放麒麟!\n");
    } else {
        printf("hello,openkylin!\n");
    }
    return 0;
}
```

4. 提交修改后的代码，推送到个人仓库中

```sh
git add src/hello.c
git commit -m "添加根据系统语言输出对应的文本"
git push origin master
```

5. 提交pr合并请求

在网页端，向openkylin/debhello仓库提交pr合并请求。

等待软件包仓库维护者对提交的合并请求进行代码审核，代码审核通过之后接受pr合并请求。

此时，开发者贡献的代码就被合并到主线代码中。

当pr合并请求成功合并之后，openkylin平台内部会自动将其打包、测试、编译。