
## competition 兴趣小组（SIG）

competition SIG组主要负责社区相关比赛优秀人才与作品的管理，包括麒麟杯大赛、开放原子开源大赛等。

## 工作目标

- 负责优秀作品与优秀人才的集中管理


## SIG成员

### Maintainers
- kangyanhong@kylinos.cn
- liumeiyan@kylinos.cn
- weiyunbo@kylinos.cn

### 面向RISC-V架构的AI开发框架构建与优化大赛
- 刘莉莉【qiuyuyufei】
- 李汉华【clxf66】
- 金宇【kingfish404】
- 陈阳【cyan-io】
- 石昌青【SCCCCQ】
- 王昭成【wangzc1022】
- 张炜【NOIOB】
- 王小宇【wxyaa】
- 欧沛鑫【pan-hongbing】
- 刘忠民【liuzmm】
- 隋浩然【sui_haoran】
- 潘祎哲【PanYizhe】
- 洪欣然【jepril】
- 徐晨晨【carson__che】
- 张淞涵【atreep】
- 翟云天【zhaiyuntian12345】
- 王昊天【Shhhhh1】
- 隋轶丞【PairRoc】
- 曹行行【balababa】


## 邮件列表
competition@lists.openkylin.top
