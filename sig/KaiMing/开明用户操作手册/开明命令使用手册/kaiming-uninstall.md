# kaiming 卸载应用

``kaiming uninstall`` 命令可以用来卸载开明应用。

查看 ``kaiming uninstall`` 命令的帮助信息：

```bash
$ kaiming uninstall 应用ID # 卸载应用
```

## 卸载示例

运行 ``kaiming uninstall`` 命令卸载开明应用。

```bash
$ kaiming uninstall org.kde.kcalc
```