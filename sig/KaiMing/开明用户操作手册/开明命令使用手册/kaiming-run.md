# kaiming 运行应用

``kaiming run`` 可以运行一个已安装的开明应用。

查看 ``kaiming run`` 的帮助信息：

```bash
$ kaiming run --help 
用法：
  kaiming run [选项…] 应用 [参数…] - 运行应用

帮助选项：
  -h, --help                              显示帮助选项
```

## 运行示例

运行  ``kaiming run`` 可以运行一个已安装的开明应用。

```bash
$ kaiming run org.kde.kcalc
```