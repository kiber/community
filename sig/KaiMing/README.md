# 开明 SIG
开明项目组的愿景是提供一个应用软件兼容，安全，隔离的解决方案。

## 工作目标

我们的工作目标主要是提供一个技术方案让应用能独立升级，隔离运行，安全权限控制，同时确保不对主机系统造成潜在的威胁和破坏： 

- 技术研究范围：

  - 隔离运行，应用程序之间，以及应用程序与主机系统之间实现运行时隔离。

  - 权限控制，应用程序动态权限细粒度控制。控制应用程序的行为，达到保护用户数据与隐私安全的目标。

  - 独立升级，应用程序独立升级，静态视角实现应用程序与主机的隔离。

  - 多平台兼容，不仅能支持原生linux桌面系统的应用，还支持Android应用，以及Windows应用。


## SIG成员


### SIG-owner
- 邢健
- 赵玉彪
- 赵民勇
- 刘聪
- 余烁琦
- 张照红

### SIG-maintainers

- @xingjian(xingjian@kylinos.cn)
- @yubiao.zhao(yubiao.zhao@cs2c.com.cn)
- @zhaominyong(zhaominyong@kylinos.cn)
- @shuoqi.yu(shuoqi.yu@cs2c.com.cn)
- @liucong1(liucong1@kylinos.cn)
- @zhangzhaohong(zhangzhaohong@kylinos.cn)

